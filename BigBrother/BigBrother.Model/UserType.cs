﻿using BigBrother.Common.BaseModels;
using System.Collections.Generic;

namespace BigBrother.Models
{
    public class UserType: BaseEntity
    {
        public string Reference { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
