﻿using BigBrother.Common.BaseModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BigBrother.Models
{
    public class BluetoothDevice: MutableEntity, IAudible
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public Guid DeviceTypeId { get; set; }
        [Timestamp]
        public byte[] Timestamp { get; set; }

        public virtual BluetoothDeviceType DeviceType { get; set; }
        public virtual ICollection<DevicePosition> Positions { get; set; }
        public virtual ICollection<RSSIBetweenDevices> ScannerDevicesRssies { get; set; }
        public virtual ICollection<RSSIBetweenDevices> DevicesRssies { get; set; }
    }
}
