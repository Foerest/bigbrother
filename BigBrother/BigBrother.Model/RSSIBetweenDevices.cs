﻿using BigBrother.Common.BaseModels;
using System;

namespace BigBrother.Models
{
    public class RSSIBetweenDevices: MutableEntity
    {
        public Guid ScannerDeviceId { get; set; }
        public Guid DeviceId { get; set; }
        public double RSSI { get; set; }

        public virtual BluetoothDevice ScannerDevice { get; set; }
        public virtual BluetoothDevice Device { get; set; }
    }
}
