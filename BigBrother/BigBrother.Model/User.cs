﻿using BigBrother.Common.BaseModels;
using System;

namespace BigBrother.Models
{
    public class User: MutableEntity
    {
        public Guid UserTypeId { get; set; }
        public string Name { get; set; }

        public virtual UserType UserType { get; set; }
    }
}
