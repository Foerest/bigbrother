﻿using BigBrother.Common.BaseModels;

namespace BigBrother.Models
{
    public class BluetoothDeviceType : BaseEntity
    {
        public string Reference { get; set; }
    }
}
