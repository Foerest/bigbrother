﻿using BigBrother.Common.BaseModels;
using System;

namespace BigBrother.Models
{
    public class DevicePosition: ImmutableEntity
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Guid DeviceId { get; set; }

        public virtual BluetoothDevice Device { get; set; }
    }
}
