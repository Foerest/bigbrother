﻿using System;

namespace BigBrother.Resources
{
    public class BLEScannerResource
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public IEquatable<BluetoothDeviceResource> BluetoothDevices { get; set; }
    }
}
