﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BigBrother.Resources
{
    public class BluetoothDeviceResource
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public double RSSI { get; set; }
    }
}
