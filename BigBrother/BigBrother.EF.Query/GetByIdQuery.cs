﻿using BigBrother.Common.Queries;
using Microsoft.EntityFrameworkCore;
using System;

namespace BigBrother.EF.Query
{
    public class GetByIdQuery<T> : IGetByIdQuery<T> where T : class
    {
        private readonly DbContext context;

        public GetByIdQuery(DbContext dbContext)
        {
            context = dbContext;
        }

        public T Execute(Guid id)
        {
            return context.Find<T>(id);
        }
    }
}
