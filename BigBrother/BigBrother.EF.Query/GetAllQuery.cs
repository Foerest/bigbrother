﻿using BigBrother.Common.Queries;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace BigBrother.EF.Query
{
    public class GetAllQuery<T>: IGetAllQuery<T> where T: class 
    {
        private readonly DbContext context;

        public GetAllQuery(DbContext dbContext)
        {
            context = dbContext;
        }

        public IQueryable<T> Execute()
        {
            return context.Set<T>();
        }
    }
}
