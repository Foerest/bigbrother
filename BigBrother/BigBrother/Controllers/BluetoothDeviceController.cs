﻿using BigBrother.Domain.Commands;
using BigBrother.Domain.Queries;
using BigBrother.Dtos;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BigBrother.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BluetoothDeviceController : ApiControllerBase
    {
        public BluetoothDeviceController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<IEnumerable<BLEScannerDto>> GetAllAsync()
        {
            return await QueryAsync(new GetBluetoothDeviceQuery());
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> PostNewRSSIForDevices([FromBody] UpdateRSSIForDevicesCommand command)
        {
            return Ok(await CommandAsync(command));
        }
    }
}
