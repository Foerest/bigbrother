﻿using BigBrother.Models;
using System.Threading.Tasks;

namespace BigBrother.Hubs
{
    public interface IDeviceHub
    {
        Task SendDeviceAsync(BluetoothDevice device);
    }
}
