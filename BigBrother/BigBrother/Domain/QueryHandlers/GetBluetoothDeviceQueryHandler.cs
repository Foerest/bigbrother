﻿using BigBrother.Data.IRepositories;
using BigBrother.Data.Seeders;
using BigBrother.Domain.Queries;
using BigBrother.Dtos;
using BigBrother.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BigBrother.Domain.QueryHandlers
{
    public class GetBluetoothDeviceQueryHandler : IRequestHandler<GetBluetoothDeviceQuery, IEnumerable<BLEScannerDto>>
    {
        private readonly IRepository<BluetoothDevice> _deviceRepository;
        private readonly ILogger<GetBluetoothDeviceQueryHandler> _logger;

        public GetBluetoothDeviceQueryHandler(IRepository<BluetoothDevice> deviceRepository, ILogger<GetBluetoothDeviceQueryHandler> logger)
        {
            _deviceRepository = deviceRepository;
            _logger = logger;
        }

        public async Task<IEnumerable<BLEScannerDto>> Handle(GetBluetoothDeviceQuery request, CancellationToken cancellationToken)
        {
            return await _deviceRepository.Queryable()
                .Where(d => d.DeviceTypeId == BigBrotherSeededData.BluetoothDeviceTypeScanner)
                .Include(d => d.DevicesRssies)
                .ThenInclude(r => r.Device)
                .Select(d => new BLEScannerDto
                {
                    Id = d.Id,
                    Address = d.Address,
                    Name = d.Name,
                    BluetoothDevices = d.DevicesRssies.Select(dr => new BluetoothDeviceDto
                    {
                        Id = dr.Id,
                        Address = dr.Device.Address,
                        Name = dr.Device.Name,
                        RSSI = dr.RSSI,
                    }),
                })
                .ToListAsync();
        }
    }
}
