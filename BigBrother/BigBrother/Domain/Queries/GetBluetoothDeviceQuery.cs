﻿using BigBrother.Dtos;
using System.Collections.Generic;

namespace BigBrother.Domain.Queries
{
    public class GetBluetoothDeviceQuery : QueryBase<IEnumerable<BLEScannerDto>>
    {
        public GetBluetoothDeviceQuery()
        {
        }
    }
}
