﻿using System;
using System.Collections.Generic;

namespace BigBrother.Dtos
{
    public class BLEScannerDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public IEnumerable<BluetoothDeviceDto> BluetoothDevices { get; set; }
    }
}
