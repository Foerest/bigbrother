﻿using System;

namespace BigBrother.Dtos
{
    public class BluetoothDeviceDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public double RSSI { get; set; }
    }
}
