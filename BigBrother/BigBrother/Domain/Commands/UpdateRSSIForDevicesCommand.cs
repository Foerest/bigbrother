﻿using BigBrother.Dtos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BigBrother.Domain.Commands
{
    public sealed class UpdateRSSIForDevicesCommand : CommandBase<BLEScannerDto>
    {
        public UpdateRSSIForDevicesCommand() { }

        [JsonConstructor]
        public UpdateRSSIForDevicesCommand(Guid? id, string name, string address, IEnumerable<BluetoothDeviceDto> bluetoothDevices)
        {
            Id = id;
            Name = name;
            Address = address;
            BluetoothDevices = bluetoothDevices;
        }

        [JsonProperty("id")]
        public Guid? Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("bluetoothDevices")]
        public IEnumerable<BluetoothDeviceDto> BluetoothDevices { get; set; }
    }
}
