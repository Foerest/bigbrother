﻿using MediatR;

namespace BigBrother.Domain.Commands
{
    public abstract class CommandBase<T> : IRequest<T> where T : class
    {

    }
}
