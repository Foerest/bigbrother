﻿using BigBrother.Data.IRepositories;
using BigBrother.Data.Seeders;
using BigBrother.Domain.Commands;
using BigBrother.Dtos;
using BigBrother.Hubs;
using BigBrother.Models;
using BigBrother.Services.Interfaces;
using MediatR;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BigBrother.Domain.CommandHandlers
{
    public class UpdateRSSIForDevicesCommandHandler : IRequestHandler<UpdateRSSIForDevicesCommand, BLEScannerDto>
    {
        private readonly IRepository<BluetoothDevice> _deviceRepository;
        private readonly IRepository<RSSIBetweenDevices> _rssiRepository;
        private readonly IBackgroundTaskQueue _backgroundTaskQueue;
        private readonly IHubContext<DeviceHub> _hubContext;
        private readonly IFindDevicePositionService _findDevicePositionService;
        private readonly ILogger<UpdateRSSIForDevicesCommandHandler> _logger;

        public UpdateRSSIForDevicesCommandHandler(IRepository<BluetoothDevice> deviceRepository,
            IRepository<RSSIBetweenDevices> rssiRepository,
            IBackgroundTaskQueue backgroundTaskQueue,
            IHubContext<DeviceHub> hubContext,
            IFindDevicePositionService findDevicePositionService,
            ILogger<UpdateRSSIForDevicesCommandHandler> logger)
        {
            _deviceRepository = deviceRepository;
            _rssiRepository = rssiRepository;
            _backgroundTaskQueue = backgroundTaskQueue;
            _findDevicePositionService = findDevicePositionService;
            _hubContext = hubContext;
            _logger = logger;
        }

        public async Task<BLEScannerDto> Handle(UpdateRSSIForDevicesCommand request, CancellationToken cancellationToken)
        {
            var scannerInDB = request.Id.HasValue ? _deviceRepository.Get(request.Id.Value) : null
                ?? _deviceRepository.Queryable().FirstOrDefault(d => d.Address == request.Address);

            if (scannerInDB == null)
            {
                scannerInDB = new BluetoothDevice
                {
                    Id = request.Id ?? Guid.NewGuid(),
                    Name = request.Name,
                    Address = request.Address,
                    DeviceTypeId = BigBrotherSeededData.BluetoothDeviceTypeScanner,
                };

                _deviceRepository.Add(scannerInDB);
            }

            var addresses = request.BluetoothDevices.Select(d => d.Address);
            var devicesInDBAddresses = _deviceRepository.Queryable()
                .Select(d => d.Address)
                .Where(a => addresses.Contains(a));

            var devicesForCreate = request.BluetoothDevices
                .Where(d => !devicesInDBAddresses.Contains(d.Address))
                .Select(d => new BluetoothDevice
                {
                    Id = d.Id,
                    Name = d.Name,
                    Address = d.Address,
                    DeviceTypeId = BigBrotherSeededData.BluetoothDeviceTypeDevice,
                });

            _deviceRepository.AddRange(devicesForCreate);

            await _deviceRepository.SaveChangesAsync();

            var devicesInDB = _deviceRepository.Queryable()
                .Include(d => d.ScannerDevicesRssies)
                .Where(d => addresses.Contains(d.Address));

            var rssiPairsForCreate = new List<RSSIBetweenDevices>();

            foreach (var device in devicesInDB)
            {
                var rssiPair = new RSSIBetweenDevices
                {
                    DeviceId = device.Id,
                    ScannerDeviceId = scannerInDB.Id,
                    RSSI = request.BluetoothDevices.First(d => d.Address == device.Address).RSSI,
                };
                device.ScannerDevicesRssies.Add(rssiPair);

                rssiPairsForCreate.Add(rssiPair);

                var calculatePositionAndNotifyTask = new Task<BluetoothDevice>(delegate
                {
                    var position = _findDevicePositionService.FindPosition(device);
                    device.Positions.Add(position);
                    return device;
                }).ContinueWith(delegate
                {
                    _hubContext.Clients.All.SendAsync(nameof(IDeviceHub.SendDeviceAsync), device);
                });

                var wrapperForTask = new Func<CancellationToken, Task>((CancellationToken ct) => calculatePositionAndNotifyTask);

                _backgroundTaskQueue.QueueBackgroundWorkItem(wrapperForTask);
            }

            _rssiRepository.AddRange(rssiPairsForCreate);

            await _rssiRepository.SaveChangesAsync();

            return await Task.FromResult(new BLEScannerDto
            {
                Id = scannerInDB.Id,
                Name = scannerInDB.Name,
                Address = scannerInDB.Address,
                BluetoothDevices = request.BluetoothDevices,
            });
        }
    }
}
