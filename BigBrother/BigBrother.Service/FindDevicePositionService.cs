﻿using BigBrother.Data.IRepositories;
using BigBrother.Models;
using BigBrother.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BigBrother.Services
{
    internal struct Point
    {
        public int x;
        public int y;

        public Point(int x, int y) : this()
        {
            this.x = x;
            this.y = y;
        }
    }

    public class FindDevicePositionService : IFindDevicePositionService
    {
        private readonly IRepository<BluetoothDevice> _devicesRepository;

        public FindDevicePositionService(IRepository<BluetoothDevice> devicesRepository)
        {
            _devicesRepository = devicesRepository;
        }

        public DevicePosition FindPosition(BluetoothDevice device)
        {
            // TODO: Not sure. Probably will be better find they through the repository
            var bluetoothScanners = device.ScannerDevicesRssies.ToArray();

            var estimatedPoints = new List<Point>();

            // O(sum(n-1))
            for (int i = 0; i < bluetoothScanners.Length - 1; i++)
            {
                var currentScanner = bluetoothScanners[i];
                for (int j = i + 1; j < bluetoothScanners.Length; j++)
                {
                    var comparableScanner = bluetoothScanners[j];

                    var rationOfRSSI = currentScanner.RSSI / comparableScanner.RSSI;
                    var scannerPosition = currentScanner.ScannerDevice.Positions.First();
                    var comparableScannerPosition = comparableScanner.ScannerDevice.Positions.First();

                    var distanceBetweenScanners = GetDistance(scannerPosition, comparableScannerPosition);
                    var estimatedDistanceToDevice = rationOfRSSI * distanceBetweenScanners;

                    var estimatedPoint = FindPointAlongTheLineByDistance(scannerPosition, comparableScannerPosition, distanceBetweenScanners, estimatedDistanceToDevice);

                    estimatedPoints.Add(estimatedPoint);
                }
            }

            // find expected value (MX)
            var devicePointX = 0;
            var devicePointY = 0;

            estimatedPoints.ForEach(p =>
            {
                devicePointX += p.x;
                devicePointY += p.y;
            });

            devicePointX /= estimatedPoints.Count;
            devicePointY /= estimatedPoints.Count;

            var devicePosition = new DevicePosition {
                X = devicePointX, 
                Y = devicePointY,
                DeviceId = device.Id,
            };

            return devicePosition;
        }

        private double GetDistance(DevicePosition a, DevicePosition b)
        {
            return Math.Sqrt((a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y));
        }

        private Point FindPointAlongTheLineByDistance(DevicePosition a, DevicePosition b, double distanceBetweenPoints, double estimatedDistanceToNewPoint)
        {
            var ratioOfDistances = estimatedDistanceToNewPoint / distanceBetweenPoints;

            int x = (int) (((1 - ratioOfDistances) * a.X) + (ratioOfDistances * b.X));
            int y = (int) (((1 - ratioOfDistances) * a.Y) + (ratioOfDistances * b.Y));

            return new Point(x, y);
        }
    }
}
