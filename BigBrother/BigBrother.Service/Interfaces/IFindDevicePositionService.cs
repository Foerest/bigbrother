﻿using BigBrother.Models;
using System.Threading.Tasks;

namespace BigBrother.Services.Interfaces
{
    public interface IFindDevicePositionService
    {
        public DevicePosition FindPosition(BluetoothDevice device);
        //public Task<DevicePosition> FindPositionAsync(BluetoothDevice device);
    }
}
