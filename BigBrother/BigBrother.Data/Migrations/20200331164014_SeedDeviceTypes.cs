﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BigBrother.Data.Migrations
{
    public partial class SeedDeviceTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "BluetoothDeviceType",
                columns: new[] { "Id", "Reference" },
                values: new object[] { new Guid("6f6512f4-6131-48a0-92d3-7e2291126770"), "Scanner" });

            migrationBuilder.InsertData(
                table: "BluetoothDeviceType",
                columns: new[] { "Id", "Reference" },
                values: new object[] { new Guid("6f6512f4-6131-48a0-92d3-7e2291126771"), "Device" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BluetoothDeviceType",
                keyColumn: "Id",
                keyValue: new Guid("6f6512f4-6131-48a0-92d3-7e2291126770"));

            migrationBuilder.DeleteData(
                table: "BluetoothDeviceType",
                keyColumn: "Id",
                keyValue: new Guid("6f6512f4-6131-48a0-92d3-7e2291126771"));
        }
    }
}
