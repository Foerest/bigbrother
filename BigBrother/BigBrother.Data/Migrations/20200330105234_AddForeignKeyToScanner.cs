﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BigBrother.Data.Migrations
{
    public partial class AddForeignKeyToScanner : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ScannedByDeviceId",
                table: "BLEDevice",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ScannedById",
                table: "BLEDevice",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BLEDevice_ScannedByDeviceId",
                table: "BLEDevice",
                column: "ScannedByDeviceId");

            migrationBuilder.AddForeignKey(
                name: "FK_BLEDevice_BLEDevice_ScannedByDeviceId",
                table: "BLEDevice",
                column: "ScannedByDeviceId",
                principalTable: "BLEDevice",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BLEDevice_BLEDevice_ScannedByDeviceId",
                table: "BLEDevice");

            migrationBuilder.DropIndex(
                name: "IX_BLEDevice_ScannedByDeviceId",
                table: "BLEDevice");

            migrationBuilder.DropColumn(
                name: "ScannedByDeviceId",
                table: "BLEDevice");

            migrationBuilder.DropColumn(
                name: "ScannedById",
                table: "BLEDevice");
        }
    }
}
