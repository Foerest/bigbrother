﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BigBrother.Data.Migrations
{
    public partial class AddDeviceTypesTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "DeviceTypeId",
                table: "BluetoothDevice",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "BluetoothDeviceType",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Reference = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BluetoothDeviceType", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BluetoothDevice_DeviceTypeId",
                table: "BluetoothDevice",
                column: "DeviceTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_BluetoothDevice_BluetoothDeviceType_DeviceTypeId",
                table: "BluetoothDevice",
                column: "DeviceTypeId",
                principalTable: "BluetoothDeviceType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BluetoothDevice_BluetoothDeviceType_DeviceTypeId",
                table: "BluetoothDevice");

            migrationBuilder.DropTable(
                name: "BluetoothDeviceType");

            migrationBuilder.DropIndex(
                name: "IX_BluetoothDevice_DeviceTypeId",
                table: "BluetoothDevice");

            migrationBuilder.DropColumn(
                name: "DeviceTypeId",
                table: "BluetoothDevice");
        }
    }
}
