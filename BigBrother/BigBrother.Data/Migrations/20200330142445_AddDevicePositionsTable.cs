﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BigBrother.Data.Migrations
{
    public partial class AddDevicePositionsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BLEDevice");

            migrationBuilder.CreateTable(
                name: "BluetoothDevice",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(nullable: false),
                    CreatedById = table.Column<Guid>(nullable: false),
                    LastModifiedAt = table.Column<DateTimeOffset>(nullable: false),
                    LastModifiedById = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: false),
                    RSSI = table.Column<double>(nullable: false),
                    ScannedById = table.Column<Guid>(nullable: true),
                    Timestamp = table.Column<byte[]>(rowVersion: true, nullable: true),
                    ScannedByDeviceId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BluetoothDevice", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BluetoothDevice_BluetoothDevice_ScannedByDeviceId",
                        column: x => x.ScannedByDeviceId,
                        principalTable: "BluetoothDevice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DevicePosition",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(nullable: false),
                    CreatedById = table.Column<Guid>(nullable: false),
                    X = table.Column<int>(nullable: false),
                    Y = table.Column<int>(nullable: false),
                    DeviceId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DevicePosition", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DevicePosition_BluetoothDevice_DeviceId",
                        column: x => x.DeviceId,
                        principalTable: "BluetoothDevice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BluetoothDevice_ScannedByDeviceId",
                table: "BluetoothDevice",
                column: "ScannedByDeviceId");

            migrationBuilder.CreateIndex(
                name: "IX_DevicePosition_DeviceId",
                table: "DevicePosition",
                column: "DeviceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DevicePosition");

            migrationBuilder.DropTable(
                name: "BluetoothDevice");

            migrationBuilder.CreateTable(
                name: "BLEDevice",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    CreatedById = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LastModifiedAt = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    LastModifiedById = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RSSI = table.Column<double>(type: "float", nullable: false),
                    ScannedByDeviceId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ScannedById = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BLEDevice", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BLEDevice_BLEDevice_ScannedByDeviceId",
                        column: x => x.ScannedByDeviceId,
                        principalTable: "BLEDevice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BLEDevice_ScannedByDeviceId",
                table: "BLEDevice",
                column: "ScannedByDeviceId");
        }
    }
}
