﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BigBrother.Data.Migrations
{
    public partial class AddRSSIBetweenDevicesTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BluetoothDevice_BluetoothDevice_ScannedByDeviceId",
                table: "BluetoothDevice");

            migrationBuilder.DropIndex(
                name: "IX_BluetoothDevice_ScannedByDeviceId",
                table: "BluetoothDevice");

            migrationBuilder.DropColumn(
                name: "RSSI",
                table: "BluetoothDevice");

            migrationBuilder.DropColumn(
                name: "ScannedByDeviceId",
                table: "BluetoothDevice");

            migrationBuilder.DropColumn(
                name: "ScannedById",
                table: "BluetoothDevice");

            migrationBuilder.CreateTable(
                name: "RSSIBetweenDevices",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(nullable: false),
                    CreatedById = table.Column<Guid>(nullable: false),
                    LastModifiedAt = table.Column<DateTimeOffset>(nullable: false),
                    LastModifiedById = table.Column<Guid>(nullable: false),
                    ScannerDeviceId = table.Column<Guid>(nullable: false),
                    DeviceId = table.Column<Guid>(nullable: false),
                    RSSI = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RSSIBetweenDevices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RSSIBetweenDevices_BluetoothDevice_DeviceId",
                        column: x => x.DeviceId,
                        principalTable: "BluetoothDevice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RSSIBetweenDevices_BluetoothDevice_ScannerDeviceId",
                        column: x => x.ScannerDeviceId,
                        principalTable: "BluetoothDevice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RSSIBetweenDevices_DeviceId",
                table: "RSSIBetweenDevices",
                column: "DeviceId");

            migrationBuilder.CreateIndex(
                name: "IX_RSSIBetweenDevices_ScannerDeviceId",
                table: "RSSIBetweenDevices",
                column: "ScannerDeviceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RSSIBetweenDevices");

            migrationBuilder.AddColumn<double>(
                name: "RSSI",
                table: "BluetoothDevice",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<Guid>(
                name: "ScannedByDeviceId",
                table: "BluetoothDevice",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ScannedById",
                table: "BluetoothDevice",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BluetoothDevice_ScannedByDeviceId",
                table: "BluetoothDevice",
                column: "ScannedByDeviceId");

            migrationBuilder.AddForeignKey(
                name: "FK_BluetoothDevice_BluetoothDevice_ScannedByDeviceId",
                table: "BluetoothDevice",
                column: "ScannedByDeviceId",
                principalTable: "BluetoothDevice",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
