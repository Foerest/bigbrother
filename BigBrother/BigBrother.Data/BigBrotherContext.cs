﻿using BigBrother.Common.BaseModels;
using BigBrother.Data.Seeders;
using BigBrother.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BigBrother.Data
{
    public class BigBrotherDbContext : DbContext
    {
        public ICollection<BluetoothDevice> BluetoothDevices { get; set; }
        public ICollection<DevicePosition> DevicePositions { get; set; }
        public ICollection<User> Users { get; set; }
        public ICollection<UserType> UserTypes { get; set; }

        public BigBrotherDbContext(DbContextOptions<BigBrotherDbContext> options)
        : base(options)
        { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BluetoothDevice>()
                .Property(b => b.Address)
                .IsRequired();

            modelBuilder.Entity<BluetoothDevice>()
                .HasMany(b => b.Positions)
                .WithOne(p => p.Device);

            modelBuilder.Entity<RSSIBetweenDevices>()
                .HasOne(b => b.ScannerDevice)
                .WithMany(d => d.ScannerDevicesRssies);

            modelBuilder.Entity<RSSIBetweenDevices>()
                .HasOne(b => b.Device)
                .WithMany(d => d.DevicesRssies);

            modelBuilder.Entity<User>()
                .HasOne(u => u.UserType)
                .WithMany(t => t.Users);

            // Seed device types
            modelBuilder.Entity<BluetoothDeviceType>().HasData(
                new BluetoothDeviceType { Id = BigBrotherSeededData.BluetoothDeviceTypeScanner, Reference = "Scanner" },
                new BluetoothDeviceType { Id = BigBrotherSeededData.BluetoothDeviceTypeDevice, Reference = "Device" });
        }

        public override int SaveChanges()
        {
            AddAuditValues();

            return base.SaveChanges();
        }

        public void DetachAllEntities()
        {
            var changedEntriesCopy = ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added ||
                            e.State == EntityState.Modified ||
                            e.State == EntityState.Deleted)
                .ToList();

            foreach (var entry in changedEntriesCopy)
                entry.State = EntityState.Detached;
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            AddAuditValues();

            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void AddAuditValues()
        {
            var entities = ChangeTracker.Entries().Where(x => (x.Entity is BaseEntity)
                && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entity in entities)
            {
                if (entity.Entity is BaseEntity && entity.State == EntityState.Added && ((BaseEntity)entity.Entity).Id == Guid.Empty)
                {
                    ((BaseEntity)entity.Entity).Id = Guid.NewGuid();
                }
                if (entity.Entity is ImmutableEntity && entity.State == EntityState.Added)
                {
                    ((ImmutableEntity)entity.Entity).CreatedAt = DateTimeOffset.Now;
                    ((ImmutableEntity)entity.Entity).CreatedById = Guid.NewGuid();
                }
                if (entity.Entity is MutableEntity && entity.State == EntityState.Added)
                {
                    ((MutableEntity)entity.Entity).LastModifiedAt = DateTimeOffset.Now;
                    ((MutableEntity)entity.Entity).LastModifiedById = Guid.NewGuid();
                }
                if (entity.Entity is MutableEntity && entity.State == EntityState.Modified)
                {
                    ((MutableEntity)entity.Entity).LastModifiedAt = DateTimeOffset.Now;
                    ((MutableEntity)entity.Entity).LastModifiedById = Guid.NewGuid();
                }
            }
        }
    }
}
