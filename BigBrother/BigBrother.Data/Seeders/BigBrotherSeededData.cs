﻿using System;

namespace BigBrother.Data.Seeders
{
    public static class BigBrotherSeededData
    {
        public static Guid BluetoothDeviceTypeScanner = Guid.Parse("6f6512f4-6131-48a0-92d3-7e2291126770");
        public static Guid BluetoothDeviceTypeDevice = Guid.Parse("6f6512f4-6131-48a0-92d3-7e2291126771");
    }
}
