﻿using BigBrother.Common.ConcurrencySafeCommands;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data.SqlClient;

namespace BigBrother.EF.Command
{
    public class ConcurrencySafeUpdateCommand<T> : IConcurrencySafeUpdateCommand<T> where T : class
    {
        private const int ReferenceConstraint = 547;
        private const int CannotInsertNull = 515;
        private const int CannotInsertDuplicateKeyUniqueIndex = 2601;
        private const int CannotInsertDuplicateKeyUniqueConstraint = 2627;
        private const int ArithmeticOverflow = 8115;
        private const int StringOrBinaryDataWouldBeTruncated = 8152;

        private readonly DbContext context;

        public ConcurrencySafeUpdateCommand(DbContext dbContext)
        {
            context = dbContext;
        }

        public bool Execute(T entity)
        {
            var entry = context.Attach<T>(entity);

            var saved = false;

            try
            {
                // Attempt to save changes to the database
                context.SaveChanges();
                saved = true;
            }
            catch (DbUpdateException ex)
            {
                var sqlException = ex.GetBaseException() as SqlException;

                var isConcurrencyException = sqlException != null
                    && (sqlException.Number == ReferenceConstraint
                        || sqlException.Number == CannotInsertNull
                        || sqlException.Number == CannotInsertDuplicateKeyUniqueIndex
                        || sqlException.Number == CannotInsertDuplicateKeyUniqueConstraint
                        || sqlException.Number == ArithmeticOverflow
                        || sqlException.Number == StringOrBinaryDataWouldBeTruncated);

                // throw an exception if it's not a concurrency one
                if (!isConcurrencyException)
                {
                    throw new NotSupportedException($"Don't know how to handle concurrency conflicts for {entry.Metadata.Name}", ex);
                }
            }
            catch (Exception ex)
            {
                throw new NotSupportedException($"Don't know how to handle concurrency conflicts for {entry.Metadata.Name}", ex);
            }

            return saved;
        }
    }
}
