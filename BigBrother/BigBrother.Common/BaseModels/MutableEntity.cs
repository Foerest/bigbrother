﻿using System;

namespace BigBrother.Common.BaseModels
{
    public class MutableEntity: ImmutableEntity
    {
        public DateTimeOffset LastModifiedAt { get; set; }
        public Guid LastModifiedById { get; set; }
    }
}
