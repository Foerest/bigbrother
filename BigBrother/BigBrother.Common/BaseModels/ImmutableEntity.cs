﻿using System;

namespace BigBrother.Common.BaseModels
{
    public class ImmutableEntity : BaseEntity
    {
        public DateTimeOffset CreatedAt { get; set; }
        public Guid CreatedById { get; set; }
    }
}
