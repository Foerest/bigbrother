﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BigBrother.Common.BaseModels
{
    public abstract class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
    }
}
